%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. фев 2014 23:01
%%%-------------------------------------------------------------------
-module(udp_client).
-author("Igor").

-define(PORT, 5678).
-define(TIMEOUT, 20000).
-define(SERVER, ?MODULE).

-behaviour(gen_server).

%% API
-export([start/0, start1/0, start2/0,
    auth1/0, auth2/0, stop/0, send/1, ping/0, auth/0, state/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-record(state, {socket, viewer, session, packet = 0}).


start() -> start(<<"1039055">>, <<"session1">>).
start1() -> start(<<"1039077">>, <<"session2">>).
start2() -> start(<<"1039088">>, <<"session3">>).
start(Viewer, Session) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [Viewer, Session], []).

stop() ->
    gen_server:call(?SERVER, stop).



state() ->
    Command = [<<"user_state">>, <<"100">>, <<"100">>, <<"50">>, <<"50">>, <<"360">>],
    send(Command).

ping() ->
    Command = [<<"ping">>],
    send(Command).

auth() -> Command = [<<"auth">>, <<"1039055">>, <<"auth_key">>, <<"1">>],  send(Command).
auth1() -> Command = [<<"auth">>, <<"1039077">>, <<"auth_key">>, <<"1">>],  send(Command).
auth2() -> Command = [<<"auth">>, <<"1039088">>, <<"auth_key">>, <<"1">>],  send(Command).




send(Message) ->
    gen_server:call(?SERVER, {send, "localhost", ?PORT, Message}).




init([Viewer, Session]) ->
    Result = gen_udp:open(0, [binary, {active, true}]),
    Reply = case Result of
                {ok, Socket} ->
                    {ok, #state{socket=Socket, viewer = Viewer, session = Session}};
                {error, Reason} ->
                    io:format("udp_server: open port error ~p~n", Reason),
                    {stop, Reason}
            end,
    Reply.


handle_call(stop, _From, State) ->
    io:format("handle call stop~n"),
    {stop, normal, stopped, State};

handle_call({send, IP, Port, Command}, _From, State) ->
    Packet = list_to_binary(integer_to_list(State#state.packet)),
    Message = [[<<"udp1">>, State#state.viewer, State#state.session, Packet], Command],
    Bin = msgpack:pack(Message),
    Reply = gen_udp:send(State#state.socket, IP, Port, Bin),
    NewPacket = State#state.packet + 1,
    {reply, Reply, State#state{packet = NewPacket}};

handle_call(Request, _From, State) ->
    io:format("handle call: ~p~n", [Request]),
    {reply, ok, State}.

handle_cast(Request, State) ->
    io:format("handle cast: ~p~n", [Request]),
    {noreply, State}.


handle_info({udp, _Socket, _IP, Port, Bin}, State) ->
    {ok, Message} = msgpack:unpack(Bin),
    io:format("handle info ~p -> ~p ~n", [Port, Message]),
    {noreply, State};
handle_info(Req, State) ->
    io:format("handle info: ~p~n", [Req]),
    {noreply, State}.


terminate(Reason, State) ->
    io:format("terminate: ~p~n", [Reason]),
    gen_udp:close(State#state.socket),
    ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.