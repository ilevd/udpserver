%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. мар 2014 20:09
%%%-------------------------------------------------------------------
-module(raw_udp_client).
-author("Igor").

-define(PORT, 5678).
-define(TIMEOUT, 2000).
-define(SERVER, ?MODULE).

%% API
-export([]).

raw_send(Message) ->
    raw_send(Message, ?PORT).
raw_send(Message, ServerPort) ->
    Result = gen_udp:open(0, [binary]),
    case Result of
        {ok, Socket} ->
            gen_udp:send(Socket, "localhost", ServerPort, Message),
            Value = receive
                        {udp, Socket, Host, Port, Bin} ->
                            {udp_client_recv, [Bin]}
                    after ?TIMEOUT ->
                        timeout
                    end,
            gen_udp:close(Socket),
            Value;
        {error, Reason} ->
            io:format("udp_client: error ~p~n", Reason)
    end.
