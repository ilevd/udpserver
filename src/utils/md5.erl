%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. мар 2014 12:25
%%%-------------------------------------------------------------------
-module(md5).
-author("Igor").

%% API
-export([md5_hex/1]).


md5_hex(S) ->
    Bin = erlang:md5(S),
    List = binary_to_list(Bin),
    Str = lists:flatten(list_to_hex(List)),
    string:to_lower(Str).

list_to_hex(L) ->
    lists:map(fun(X) -> integer_to_list(X, 16) end, L).


