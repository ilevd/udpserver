%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. мар 2014 12:07
%%%-------------------------------------------------------------------
-module(auth).
-author("Igor").

%% API
-export([run/7]).

run(IP, Port, ViewerID, SessionID, PacketID, AuthKey, SN) ->
    case correct_auth(ViewerID, AuthKey) of
        % todo: auth
        _ -> % true ->
            user_manager:terminate_clients(ViewerID, SessionID),
            user_manager:add_client(IP, Port, ViewerID, SessionID, PacketID, SN)
        %_ ->
        %    not_auth
    end,
    ok.

correct_auth(ViewerID, AuthKey) ->
    ServAuthKey = md5:md5_hex(integer_to_list(settings:api_id()) ++ "_" ++
        integer_to_list(ViewerID) ++ "_" ++ settings:secret_key()),
    AuthKey =:= ServAuthKey.