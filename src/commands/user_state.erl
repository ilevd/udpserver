%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. мар 2014 12:07
%%%-------------------------------------------------------------------
-module(user_state).
-author("Igor").

%% API
-export([run/4]).

run(ClientPid, ViewerID, PacketID, [_UserStateNameCommand|Command]) ->
    case client:is_next_packet(ClientPid, PacketID) of
        true ->
            ClientPids = user_manager:get_clients(),
            Message = [<<"user_state">>, ViewerID | Command],
            PackMessage = msgpack:pack(Message),
            lists:map(fun(Pid) -> send_user_state(ClientPid, Pid, PackMessage) end, ClientPids);
        _ ->
            io:format("old packet~n")
    end.

send_user_state(_ClientPid, Pid, Message) ->
    {IP, Port} = client:get_addr(Pid),
    io:format("user_state send: ~p~p~n", [IP, Port]),
    udp_server:send(IP, Port, Message).