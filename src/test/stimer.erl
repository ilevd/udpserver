%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 03. мар 2014 21:46
%%%-------------------------------------------------------------------
-module(stimer).
-author("Igor").
-compile(export_all).

%% API
-export([]).


start(Time, Fun) -> Pid = spawn(fun() -> timer(Time, Fun) end),
    register(st, Pid).

cancel() -> st ! stop.
timer(Time, Fun) ->
    receive
        stop -> void
        after Time ->
            Fun(),
            timer(Time, Fun)
    end.