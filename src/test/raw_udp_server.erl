%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%s
%%% @end
%%% Created : 26. фев 2014 21:24
%%%-------------------------------------------------------------------
-module(raw_udp_server).
-author("Igor").

%% API
-export([start/0]).

-define(PORT, 4567).

start() ->
    Result = gen_udp:open(?PORT, [binary, {active, false}]),
    case Result of
        {ok, Socket} ->
            loop(Socket);
        {error, Reason} ->
            io:format("udp_server: open port error ~p~n", Reason),
            Reason
    end.


loop(Socket) ->
    inet:setopts(Socket, [{active, once}]),
    receive
        {udp, Socket, Host, Port, Bin} ->
            io:format("udp_server: recv: ~p~n", [Bin]),
            gen_udp:send(Socket, Host, Port, Bin),
            loop(Socket);
        SomeMessage ->
            io:format("upd_server: recv some msg: ~p~n", SomeMessage)
    end.

