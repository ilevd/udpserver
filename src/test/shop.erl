%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. фев 2014 19:54
%%%-------------------------------------------------------------------
-module(shop).
-author("Igor").

%% API
-export([total/1, start/0]).
-compile(export_all).

bson() ->
    S = "{\"Hello\":\"igor\"}",
    emongo_bson:encode(S).


total([{F, N} | T]) -> c(F) * N + total(T);
total([]) -> 0.

start() ->
    io:format("hello world").

c(_) -> 5.