%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. фев 2014 23:06
%%%-------------------------------------------------------------------
-module(main).
-author("Igor").

%% API
-export([area/1, z/0, max/3, rec/1]).
-include("../include/records.hrl").
-compile(export_all).

rec(X = Y) ->
    R = #person{},
    #person{name = Name, age = Age} = R,
    %% R#person.name.
    R.

rpc(Pid, Req) ->
    Pid ! {self(), Req},
    receive
        {Pid, Resp} -> Resp
    end.



loop() ->
    receive
        {From, X} ->
            From ! {self(), area(X)};
        _ -> error
    end,
    loop().

area({rect, W, H}) -> W * H;
area({circle, R}) -> 3.14159 * R * R;
area(_) -> error.

max(X, Y, Z) when X > Y; X > Z -> X;
max(_, _, _) -> no.

z() -> shop:total([{a, 3}, {b, 4}]).