%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 27. фев 2014 19:27
%%%-------------------------------------------------------------------
-module(ex).
-author("Igor").

%% API
-export([gen/1, start/0, start2/0]).


gen(1) -> a;
gen(2) -> throw(a);
gen(3) -> exit(a);
gen(4) -> {'EXIT', a};
gen(5) -> erlang:error(a).

start2() ->
    [{I, catch gen(I)} || I <- [1,2,3,4,5]].

start() ->
    [run(I) || I <- [1,2,3,4,5]].


run(N) ->
    try gen(N) of
        Val -> {N, normal, Val}
    catch
        throw:X -> {N, thrown, X};
        exit:X -> {N, exited, X};
        error:X -> {N, error, X}
    end.