%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. мар 2014 13:34
%%%-------------------------------------------------------------------
-module(client).
-author("Igor").

-behaviour(gen_server).

%% API
-export([start/6, stop/1, get_addr/1, get_user/1, req/1, get_viewer_id/1, ping/1,
        is_next_packet/2]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).
-define(TIMEOUT, 50000).
-define(MAX_PACKET, 1024).

-record(state, {user, user_info, timer}).
-record(user, {ip, port, viewerID, sessionID, packetID, sn}).
-record(user_info, {first_name, last_name, sn, bdate, photo}).



start(IP, Port, ViewerID, SessionID, PacketID, SN) ->
    User = #user{ip = IP, port = Port, viewerID = ViewerID,
        sessionID = SessionID, packetID = PacketID, sn = SN},
    State = #state{user = User},
    gen_server:start_link(?MODULE, State, []).

stop(Pid) ->
    gen_server:call(Pid, stop).

req(Pid) ->
    gen_server:call(Pid, req).

get_addr(Pid) ->
    gen_server:call(Pid, {get_addr}).

get_user(Pid) ->
    gen_server:call(Pid, {get_user}).

get_viewer_id(Pid) ->
    gen_server:call(Pid, {get_viewer_id}).

ping(Pid) ->
    gen_server:call(Pid, {ping}).

is_next_packet(Pid, PacketID) ->
    gen_server:call(Pid, {is_next_packet, PacketID}).

init(State) ->
    Timer = erlang:send_after(?TIMEOUT, self(), timer),
    NewState = State#state{timer = Timer},
    {ok, NewState}.


handle_call({get_addr}, _From, State) ->
    io:format("client get_addr: ~p~n", [State#state.user]),
    IP = State#state.user#user.ip,
    Port = State#state.user#user.port,
    {reply, {IP, Port}, State};

handle_call({get_user}, _From, State) ->
    {reply, State#state.user, State};

handle_call({get_viewer_id}, _From, State) ->
    {reply, State#state.user#user.viewerID, State};

handle_call({ping}, _From, State) ->
    erlang:cancel_timer(State#state.timer),
    NewState = State#state{timer = erlang:send_after(?TIMEOUT, self(), timer)},
    {reply, ok, NewState};

handle_call({is_next_packet, NewPacketID}, _From, State) ->
    PacketID = State#state.user#user.packetID,
    {Reply, ResState} = if
        ((NewPacketID > PacketID) and (NewPacketID - PacketID =< ?MAX_PACKET/2) ) or
        ((PacketID > NewPacketID) and (PacketID - NewPacketID > ?MAX_PACKET/2) ) ->
            User = State#state.user#user{packetID = NewPacketID},
            NewState = State#state{user = User},
            {true, NewState};
        true -> {false, State}
    end,
    {reply, Reply, ResState};

handle_call(stop, _From, State) ->
    {stop, normal, reply, State};
handle_call(req, _From, State) ->
    {reply, ok, State};
handle_call(_Request, _From, State) ->
    {reply, ok, State}.


handle_cast(_Request, State) ->
    {noreply, State}.

handle_info(timer, State) ->
    %Timer = State#state.timer,
    user_manager:delete_client(self()),
    {stop, normal, State};
%handle_info(timeout, State) ->
%    io:format("user handle_info timeout~n"),
%    user_manager:delete_client(self()),
%    {stop, normal, State};

handle_info(Info, State) ->
    io:format("user handle_info ~p~n", [Info]),
    {noreply, State}.


terminate(_Reason, _State) ->
    io:format("client terminate~n"),
    ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.
