%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. мар 2014 1:00
%%%-------------------------------------------------------------------
-module(udp_server).
-author("Igor").

-behaviour(gen_server).

%% API
-export([start/0, stop/0, send/3]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(PORT, 5678).
-define(SERVER, ?MODULE).

-record(server_state, {socket}).

start() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

stop() ->
    gen_server:call(?SERVER, stop).

send(IP, Port, Message) ->
    gen_server:cast(?SERVER, {send, IP, Port, Message}).

init([]) ->
    Result = gen_udp:open(?PORT, [binary, {active, true}]),
    Reply = case Result of
        {ok, Socket} ->
            {ok, #server_state{socket=Socket}};
        {error, Reason} ->
            io:format("udp_server: open port error ~p~n", Reason),
            {stop, Reason}
    end,
    Reply.


handle_call(stop, _From, State) ->
    io:format("handle call stop~n"),
    {stop, normal, stopped, State};
handle_call(Request, _From, State) ->
    io:format("handle call: ~p~n", [Request]),
    {reply, ok, State}.

handle_cast({send, IP, Port, Message}, State) ->
    gen_udp:send(State#server_state.socket, IP, Port, Message),
    {noreply, State};
handle_cast(Request, State) ->
    io:format("handle cast: ~p~n", [Request]),
    {noreply, State}.


handle_info({udp, _Socket, IP, Port, Bin}, State) ->
    io:format("handle info-> ~p ~p~n", [Bin, Port]),
    recv_manager:recv(IP, Port, Bin),
    % gen_udp:send(Socket, IP, Port, Bin),
    % gen_udp:send(Socket, IP, 3456, Bin),
    {noreply, State};

handle_info(Req, State) ->
    io:format("handle info: ~p~n", [Req]),
    {noreply, State}.


terminate(Reason, State) ->
    io:format("terminate: ~p~n", [Reason]),
    Socket = State#server_state.socket,
    gen_udp:close(Socket),
    ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

