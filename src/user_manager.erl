%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. мар 2014 12:09
%%%-------------------------------------------------------------------
-module(user_manager).
-author("Igor").

-behaviour(gen_server).

%% API
-export([start/0, stop/0, add_client/6, delete_client/1, get_clients/0,
    terminate_clients/2, get_client/3]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).


start() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

stop() ->
    gen_server:call(?SERVER, stop).

terminate_clients(ViewerID, SessionID) ->
    gen_server:call(?SERVER, {terminate_clients, ViewerID, SessionID}).

add_client(IP, Port, ViewerID, SessionID, PacketID, SN) ->
    gen_server:call(?SERVER, {add_client, IP, Port, ViewerID, SessionID, PacketID, SN}).

delete_client(ClientPid) ->
    gen_server:call(?SERVER, {delete_client, ClientPid}).

get_clients() ->
    gen_server:call(?SERVER, {get_clients}).

get_client(IP, Port, ViewerID) ->
    gen_server:call(?SERVER, {get_client, IP, Port, ViewerID}).


%% -------- callback funcs --------s

init([]) -> {ok, []}.


handle_call({get_clients}, _From, State) ->
    {reply, State, State};

handle_call({terminate_clients, ViewerID, SessionID}, _From, State) ->
    {reply, ok, State};

handle_call({add_client, IP, Port, ViewerID, SessionID, PacketID, SN}, _From, State) ->
    case client:start(IP, Port, ViewerID, SessionID, PacketID, SN) of
        {ok, ClientPid} ->
            NewState = [ClientPid|State],
            io:format("user_manager add: ~p~n", [NewState]),
            {reply, ok, NewState};
        Error ->
            io:format("Error start client: ~p~n", [Error]),
            {reply, Error, State}
    end;

handle_call({delete_client, ClientPid}, _From, State) ->
    NewState = lists:delete(ClientPid, State),
    io:format("user_manager delete client, now:~p~n", [NewState]),
    {reply, ok, NewState};

handle_call({get_client, IP, Port, ViewerID}, _From, State) ->
    Func = fun(X) ->
        case client:get_user(X) of
            {user, IP, Port, ViewerID, _, _, _} -> true;
            _ -> false
        end
    end,
    Pid = case lists:filter( Func, State) of
              [P] -> P;
              [] -> error
          end,
    {reply, Pid, State};

handle_call(stop, _From, State) ->
    io:format("user_manager handle_call stop~n"),
    {stop, normal, stopped, State};

handle_call(Request, _From, State) ->
    io:format("handle call: ~p~n", [Request]),
    {reply, ok, State}.





handle_cast(Request, State) ->
    io:format("handle cast: ~p~n", [Request]),
    {noreply, State}.

handle_info(Req, State) ->
    io:format("user_manager handle_info: ~p~n", [Req]),
    {noreply, State}.

terminate(Reason, State) ->
    io:format("user_manager terminate: ~p~n", [Reason]),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.




