%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. мар 2014 19:36
%%%-------------------------------------------------------------------
-module(recv_manager).
-author("Igor").

%% API
-export([recv/3]).
% -include("include/records.hrl").
-compile(export_all).

recv(IP, Port, Bin) when is_binary(Bin) ->
    try msgpack:unpack(Bin) of
        {ok, [UdpInfo, BinCommand]} ->
            recv_command({IP, Port},
                binary_to(UdpInfo),
                binary_to_command(BinCommand)
            );
        Smth ->
            io:format("Unpack another: ~p~n", [Smth])
    catch
        H:R -> io:format("UnpackError: ~p~p~n", [H,R])
    end.


recv_command({IP, Port}, [_UdpID, ViewerID, SessionID, PacketID] = UdpInfo, Command) ->
    io:format("recv_manager command: ~p ~p~n", [UdpInfo, Command]),
    Res = case Command of
        [auth, ViewerID, AuthKey, SN] -> auth:run(IP, Port, ViewerID, SessionID, PacketID, AuthKey, SN);
        _ -> case user_manager:get_client(IP, Port, ViewerID) of
                 Pid when is_pid(Pid) -> do_command(Command, UdpInfo, Pid);
                 error -> not_auth
            end
    end,
    io:format("Parse: ~p~n", [Res]).

do_command(Command, [_UdpID, ViewerID, SessionID, PacketID], Pid) ->
    case Command of
        [ping] -> ping:run(Pid);
        [update_info] -> update_info;
        [get_game_list] -> get_game_list;
        [enter_room, RoomID] -> ok;
        [select_team, TeamID] -> ok;
        [exit_game] -> ok;
        [user_state, Y, X, Vy, Vx, A] -> user_state:run(Pid, ViewerID, PacketID, Command);
        [shoot, Y, X, Vy, Vx, A] -> ok;
        _UnknownCommand -> unknownCommand
    end.


% converrt [<<"auth">>, <<"5.2">>, <<"6">>, <<"sdg">>] -> [auth, 5.2, 6, "sdg"]
binary_to_command([H|T] = List) when is_binary(H) ->
    try binary_to_atom(H, utf8) of
        Atom -> [Atom|binary_to(T)]
    catch
        _:_ -> List
    end;
binary_to_command(X) -> X.

binary_to([H|T]) -> [(bin_to_num_or_str(H))|binary_to(T)];
binary_to([]) -> [].

bin_to_num_or_str(Bin) when is_binary(Bin)->
    try binary_to_float(Bin) of
        F -> F
    catch
        _:_ ->
            try binary_to_integer(Bin) of
                I -> I
            catch
                _:_ -> binary_to_list(Bin)
            end
    end.