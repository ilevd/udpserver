%%%-------------------------------------------------------------------
%%% @author Igor
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. фев 2014 21:44
%%%-------------------------------------------------------------------
-author("Igor").

-record(person, {name = "John", age = 20}).


-record(udp_packet, {udpID, viewerID, sessionID, packetID, command}).
-record(auth, {viewerID, authKey, sn}).